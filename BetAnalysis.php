<?php
class BetAnalysis
{
	
	public $customerId;
	public $stake;
	public $toWin;
	public $unusual10Times;
	public $unusual30Times;
	public $unusualHighAmount;

	
	public function setHighAmountToWinBet()
	{
		if ($this->toWin > 1000)
			$this->unusualHighAmount = true;
	}
	
	public function setUnusual10TimesBet($averageStake)
	{
		if (($this->stake / $averageStake) > 10)
			$this->unusual10Times = true;
	}
	
	public function setUnusual30TimesBet($averageStake)
	{
		if (($this->stake / $averageStake) > 30)
			$this->unusual30Times = true;
	}
	
	
}
	?>