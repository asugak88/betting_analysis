<?php
include 'BetInfo.php';
require_once 'BetInfoData.php';
require_once 'DataAnalysis.php';
require_once 'BetAnalysis.php';

$filetype = $_POST;
$betInfoData = new BetInfoData();
$unsettledBetInfoData = new BetInfoData();
$dataAnalysis = Array();
$betAnalysis = Array();

if ($_FILES['settledBets']['error'] > 0)
  {
  echo "Error: " . $_FILES["settledBets"]["error"] . "<br />";
  }
else
  {
	  loadFile("settledBets", true, $betInfoData);
	
	foreach ($betInfoData->GetBetInfo() as &$value)
	{
		$customerData = new DataAnalysis();
		$customerData->customerId = $value->customerId;
		$dataAnalysis[$value->customerId] = $customerData;
		
	}
		
	foreach ($betInfoData->GetBetInfo() as &$value)
	{
		$dataAnalysis[$value->customerId]->IncreaseNumberOfBets();
		if ($value->won > 0)
			$dataAnalysis[$value->customerId]->IncreaseNumberOfWonBets();
		
		$dataAnalysis[$value->customerId]->IncreaseTotalOfStake($value->stake);

	}
	foreach ($betInfoData->GetBetInfo() as &$value)
	{
		$dataAnalysis[$value->customerId]->CalculateTotalStakeAverage();
		$dataAnalysis[$value->customerId]->CheckUnusualCustomerWin();
	}
	
	
	echo "Settled Bets Risk analysis<br/>";
	echo build_table($dataAnalysis, true);
	echo "<br /><br />";
	
	//print_r($dataAnalysis);
	
	loadFile("unsettledBets", false, $unsettledBetInfoData);
	
	foreach ($unsettledBetInfoData->GetBetInfo() as &$value)
	{
		$singleBetInfo = new BetAnalysis();
		$singleBetInfo->customerId = $value->customerId;
		$singleBetInfo->stake = $value->stake;
		$singleBetInfo->toWin = $value->toWin;
		$singleBetInfo->setHighAmountToWinBet();
		$singleBetInfo->setUnusual10TimesBet($dataAnalysis[$value->customerId]->totalAverage);
		$singleBetInfo->setUnusual30TimesBet($dataAnalysis[$value->customerId]->totalAverage);
		
		$betAnalysis[] = $singleBetInfo;
	}
	
	echo "Unsettled Bets Risk analysis<br />";
	
	echo build_table($betAnalysis, false);
	
	//print_r($betAnalysis);
  }
  function loadFile($fileName, $isSettled, $betInfoData)
  {
	$file = fopen($_FILES[$fileName]['name'],"r");

	while(! feof($file))
	{
		$dataLine = fgetcsv($file);
		$betInfo = new BetInfo();
		$betInfo->customerId = $dataLine[0];
		$betInfo->eventId = $dataLine[1];
		$betInfo->participantId = $dataLine[2];
		$betInfo->stake = $dataLine[3];
		$isSettled ? $betInfo->won = $dataLine[4] : $betInfo->toWin = $dataLine[4];
		if(is_numeric($betInfo->customerId))	// To avoid first line getting in
			$betInfoData->AddBetInfo($betInfo);
	}
	fclose($file);
  }
  
  
  
   function build_table($array, $isSettled){
    // start table
    $html = '<table  border="1"> ';
    // header row
    $html .= '<tr>';
    
	if ($isSettled)
	{
		$html .= '<th>CustomerId</th>';
		$html .= '<th>Number of Bets</th>';
		$html .= '<th>Number of won Bets</th>';
		$html .= '<th>Total of Stake</th>';
		$html .= '<th>Total average</th>';
		$html .= '<th>Unusual customer win</th>';
	}
	else
	{
		$html .= '<th>CustomerId</th>';
		$html .= '<th>Stake</th>';
		$html .= '<th>Money To Win</th>';
		$html .= '<th>10x higher bet</th>';
		$html .= '<th>30x higher bet</th>';
		$html .= '<th>More than $1k to win</th>';
	}
	
    $html .= '</tr>';

    // data rows
    foreach( $array as $key=>$value){
        $html .= '<tr>';
        foreach($value as $key2=>$value2){
            $html .= '<td><center>' . $value2 . '</center></td>';
        }
        $html .= '</tr>';
    }

    // finish table and return it

    $html .= '</table>';
    return $html;
}
  
  
  
?>