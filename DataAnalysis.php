<?php
class DataAnalysis
{
	
	public $customerId;
	public $numberOfBets;
	public $numberOfWonBets;
	public $totalOfStake;
	public $totalAverage;
	public $unusualCustomerWin;
	// Add few bools
	
	
	
	public function CheckUnusualCustomerWin()
	{
		if (($this->numberOfWonBets / $this->numberOfBets) > 0.6)
			$this->unusualCustomerWin = true;
		
	}
	public function CalculateTotalStakeAverage()
	{
		$this->totalAverage = $this->totalOfStake / $this->numberOfBets;
	}
	
	public function IncreaseTotalOfStake($stake)
	{
		$this->totalOfStake += $stake;
	}
	
	public function IncreaseNumberOfBets()
	{
		$this->numberOfBets++;
	}
	
	public function IncreaseNumberOfWonBets()
	{
		$this->numberOfWonBets++;
	}
}
?>