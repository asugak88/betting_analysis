Alex Sugak - Technical Test - Bet Risk Analysis

Introduction

This tool was written in PHP, due to the size and low complexity of the application. 
It was unnecessary to overcomplicate the process, if a simple solution is available(KISS principle).
The final result was achieved without using Microsoft frameworks or databases.

The solution is far from being ideal and there are multiple things that could be improved and done differently(User interface, structure of classes, OO principles, optimization and better testing),
due to time constraints and personal circumstances not all ideas and best practices could be implemented. However, I provided a working solution that meets the technical requirements before the deadline. 
I'm more than happy to discuss the solution, to hear criticism and talk about improvements that I could make.


System Requirements

WEB BROWSER - tested of Chrome 50.0.2661.102
PHP 5.5.12
APACHE 2.4.9


Deployment Instructions

WAMP server is recommended for running the application(http://www.wampserver.com/en/)
Once WAMP server is installed and running copy the content of repository to %dir%:\wamp\www\Betting directory.
Access the index file to use the tool.


For any additional information, feel free to contact me: asugak88@gmail.com